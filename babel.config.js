module.exports = {
	ignore: [ /[\/\\]core-js/, /@babel[\/\\]runtime/ ],
	presets: [ [ '@babel/preset-env' ] ],
};
