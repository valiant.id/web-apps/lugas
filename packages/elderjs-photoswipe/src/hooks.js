const { minify } = require( 'terser' );
const rollup = require( 'rollup' );

const createGalleryScript = require( './gallery' );
const createRollupConfig = require( './rollup' );

module.exports = [
	{
		hook: 'bootstrap',
		name: 'photoswipe_create_script',
		description: 'Create PhotoSwipe script.',
		run: async ( { data, settings } ) => {
			const config = createRollupConfig( `${ settings.distDir }/_assets` );
			const { output: outputConfig, ...inputConfig } = config;

			const bundle = await rollup.rollup( inputConfig );
			const result = await bundle.write( outputConfig );
			await bundle.close();

			const { fileName } = result.output[ 0 ];
			const script = createGalleryScript( `/_assets/${ fileName }` );

			let photoswipe_script;

			if ( process.env.NODE_ENV === 'production' ) {
				const minified = await minify( script );
				photoswipe_script = minified.code;
			} else {
				photoswipe_script = script;
			}

			return {
				data: {
					...data,
					photoswipe_script,
				},
			};
		},
	},
	{
		hook: 'stacks',
		name: 'photoswipe_inject_gallery_script',
		description: 'Inject gallery script before closing </body>.',
		run: async ( { data, footerStack } ) => {
			return {
				footerStack: [
					...footerStack,
					{
						string: `<script>${ data.photoswipe_script }</script>`,
						source: 'photoswipe_inject_gallery_script',
					},
				],
			};
		},
	},
];
