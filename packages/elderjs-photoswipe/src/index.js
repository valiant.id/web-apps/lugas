const hooks = require( './hooks' );

const photoswipe = {
	hooks,
	name: '@valiant.id/elderjs-photoswipe',
	description: 'Photoswipe for Elder.js',
	config: {
		selectors: {
			'.gallery': {},
		},
	},
	init: plugin => {
		return plugin;
	},
};

module.exports = photoswipe;
exports.default = photoswipe;
