const path = require( 'path' );
const { nodeResolve } = require( '@rollup/plugin-node-resolve' );
const { terser } = require( 'rollup-plugin-terser' );
const commonjs = require( '@rollup/plugin-commonjs' );

const is_prod = process.env.NODE_ENV === 'production';

function createRollupConfig( outputDir ) {
	return {
		input: path.resolve( __dirname, 'photoswipe.js' ),
		output: {
			dir: outputDir,
			entryFileNames: '[name]-[hash].js',
			format: 'esm',
			plugins: [ is_prod && terser() ],
		},
		plugins: [
			commonjs(),
			nodeResolve( {
				browser: true,
				dedupe: [ 'core-js' ],
				preferBuiltins: true,
			} ),
		],
	};
}

module.exports = createRollupConfig;
