module.exports = [
	{
		hook: 'data',
		name: 'dz_prism',
		description: 'Apply syntax highlight to content with PrismJS.',
		priority: 70,
		run: async ( { request, data, plugin } ) => {
			const { processor } = plugin;

			function highlight( content ) {
				return content.replace( /(<pre.*<\/pre>)/gms, ( _, p1 ) => processor.processSync( p1 ).toString() );
			}

			if ( [ 'page', 'post' ].includes( request.route ) ) {
				data.article.content.rendered = highlight( data.article.content.rendered );
			} else {
				data.articles = data.articles.map( article => ( {
					...article,
					content: {
						...article.content,
						rendered: highlight( article.content.rendered ),
					},
				} ) );
			}

			return { data };
		},
	},
];
