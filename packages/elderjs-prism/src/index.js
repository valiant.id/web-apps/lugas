const hooks = require( './hooks' );
const { get_processor } = require( './processor' );

const prism = {
	hooks,
	name: '@valiant.id/elderjs-prism',
	description: '...',
	config: {
		ignoreMissing: false,
	},
	init: plugin => {
		plugin.processor = get_processor( { ignoreMissing: plugin.config.ignoreMissing } );

		return plugin;
	},
};

module.exports = prism;
exports.default = prism;
