module.exports = {
	all: ( { data, helpers } ) =>
		helpers.dzwp_generate_home_routes( data.articles.length, data.wp_info.settings.archive.per_page ),
	data: ( { data, helpers, request } ) => ( {
		articles: helpers.dzwp_get_home_route_posts(
			data.articles,
			request.page,
			data.wp_info.settings.archive.per_page,
		),
		doc_title: helpers.dzwp_generate_doc_title( request.page, data.wp_info.name, data.wp_info.description ),
	} ),
	permalink: ( { request } ) => request.permalink,
};
