module.exports = {
	all: ( { data, helpers } ) => helpers.dzwp_generate_singular_routes( data.articles ),
	data: ( { data, helpers, request } ) => {
		const article = data.articles.find( i => i.slug === request.slug );

		return {
			article,
			doc_title: helpers.dzwp_generate_doc_title(
				request.page,
				data.wp_info.name,
				data.wp_info.description,
				article,
			),
		};
	},
	permalink: ( { request } ) => request.permalink,
};
